
let truckContent = document.getElementById('truckContent')

let tipoTruck = truckContent.getAttribute('data-type');
let numEjesPrimero = truckContent.getAttribute('data-ejes-first');
let numEjesSegundo = truckContent.getAttribute('data-ejes-last');

let contentEjesPrimero = document.getElementById('contentEjesPrimero')
let contentEjesSegundo = document.getElementById('contentEjesSegundo')


crearEjesPrimero(numEjesPrimero);
crearEjesSegundo(numEjesSegundo);

function crearEjesPrimero(ejes){
	for (let i = 0; i  < ejes ; i++) {
		let div = document.createElement("div");
		div.innerHTML =  `<img src="../general/eje.svg" class="truck-img truck-ejes-img">`
		contentEjesPrimero.appendChild(div);
	}
}

function crearEjesSegundo(ejes){
	for (let i = 0; i  < ejes ; i++) {
		let div2 = document.createElement("div");
		div2.innerHTML =  `<img src="../general/eje.svg" class="truck-img truck-box-last--is-eje">`
		contentEjesSegundo.appendChild(div2);
	}
}


const intEje = 35;
const intMaxEjes = parseInt(9*intEje)

let sizeEjes = intEje*numEjesSegundo
if(sizeEjes<intMaxEjes){
	let boxLastchild = document.getElementsByClassName('truck-box-last-images')[0];
	let letterE = boxLastchild.offsetWidth - (sizeEjes/2)
	var letraE = document.getElementById('medidaE')
	letraE.style.width = letterE + "px"
}else{
	var letraE = document.getElementById('medidaE')
	letraE.style.width = (sizeEjes/2) + "px"
}

