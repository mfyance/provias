
let truckContent = document.getElementById('truckContent')

let tipoTruck = truckContent.getAttribute('data-type');

let numEjesPrimero = truckContent.getAttribute('data-ejes-first');
let numEjesMitad = truckContent.getAttribute('data-ejes-middle');
let numEjesFinal = truckContent.getAttribute('data-ejes-last');

let contentEjesPrimero = document.getElementById('contentEjesPrimero');
let contentEjesMitad = document.getElementById('contentEjesMedio');
let contentEjesFinal = document.getElementById('contentEjesFinal');


crearEjesPrimero(numEjesPrimero);
crearEjesMitad(numEjesMitad);
crearEjesFinal(numEjesFinal);

function crearEjesPrimero(ejes) {
  for (let i = 0; i < ejes; i++) {
    let div = document.createElement("div");
    div.innerHTML = `<img src="../general/eje.svg" class="truck-img truck-ejes-img">`
    contentEjesPrimero.appendChild(div);
  }
}

function crearEjesMitad(ejes) {
  for (let i = 0; i < ejes; i++) {
    let div = document.createElement("div");
    div.innerHTML = `<img src="../general/eje.svg" class="truck-img truck-box-last--is-eje">`
    contentEjesMitad.appendChild(div);
  }
}

function crearEjesFinal(ejes) {
  let medida = 345
  for (let i = 0; i < ejes; i++) {
    let div2 = document.createElement("div");
    div2.innerHTML = `<img src="../general/eje.svg" class="truck-img truck-box-last--is-eje">`
    contentEjesFinal.appendChild(div2);
    if (ejes >= 2 && i != 0) {
      medida -= 35
    }
  }

}

const intEje = 30;
const intMaxEjes = parseInt(9 * intEje)

let sizeEjes = intEje * numEjesFinal
if (sizeEjes < intMaxEjes) {
  let boxLastchild = document.getElementsByClassName('truck-box-last-images')[0];
  let letterG = boxLastchild.offsetWidth - (sizeEjes / 2)
  var letraG = document.getElementById('medidaG')
  letraG.style.width = letterG + "px"
} else {
  var letraG = document.getElementById('medidaG')
  letraG.style.width = (sizeEjes / 2) + "px"
}

