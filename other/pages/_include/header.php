<header class="cabecera">
    <div class="cabecera-left cleaner">
        <div class="cabecera-logo">
            <img src="../../images/main/provias.png" alt="" class="w-100">
        </div>        
    </div>
    <div class="cabecera-right">
        <i onclick="EventoSidebar(event)" class="material-icons icon-menu">menu</i>
        <!-- <img src="../../images/main/informa-white.png" alt="informa" class="logo-informa"> -->
        <div  class="cleaner cabecera-icons">
            <i class="material-icons icon-notifications" onclick="(function(){ fn.notification.show('wc-notificacion') })()">notifications</i>

            <wc-notificacion class="header-notification" onmouseleave="(function(){ fn.notification.hide(event) })()">
                <ul class="header-notification-ul">
                    <li class="header-notification-li"><a href="javascript:void(0)">Modificación de datos de encuestadora...<span class="header-notification-moredetails">10/20/2019</span> </a></li>
                    <li class="header-notification-li"><a href="javascript:void(0)">Subsanación de informe de encuestado...<span class="header-notification-moredetails">10/20/2019</span> </a></li>
                    <li class="header-notification-li"><a href="javascript:void(0)">Modificación de datos de encuestadora...<span class="header-notification-moredetails">10/20/2019</span> </a></li>
                    <li class="header-notification-li"><a href="javascript:void(0)">Subsanación de informe de encuestado...<span class="header-notification-moredetails">10/20/2019</span> </a></li>
             
                </ul>
            </wc-notificacion>
        </div>

        <div onclick="(function(){ fn.profile.show('wc-perfil') })()" class="perfil cleaner">
            <div class="perfil-foto" style="background-image: url(../../images/main/avatar.jpg)"> </div>
            <div class="perfil-nombre" id="mostrar">
                <strong>Huber Barreto</strong>
            </div>
            <i class="material-icons m-l-10">arrow_drop_down</i>
            
            <wc-perfil class="header-profile" onmouseleave="(function(){ fn.profile.hide(event) })()">
                <ul class="header-profile-ul">
                    <li class="header-profile-li"><a href="#"><i class="header-profile-li--is-icon material-icons">send</i>Ver perfil</a></li>
                    <li class="header-profile-li"><a href="#"><i class="header-profile-li--is-icon material-icons">send</i>Agregar casilla electrónica</a></li>
                    <li class="header-profile-li"><a href="#"><i class="header-profile-li--is-icon material-icons">send</i>Cancelar inscripción</a></li>
                    <li class="header-profile-li"><a href="#"><i class="header-profile-li--is-icon material-icons">send</i>Ayuda</a></li>
                    <li class="header-profile-li"><a href="javascript:void(0)" id="CerrarSesion"><i class="header-profile-li--is-icon material-icons">send</i>Cerrar sesión</a></li>
                </ul>
            </wc-perfil>            
        </div>
    </div>
    
</header>
