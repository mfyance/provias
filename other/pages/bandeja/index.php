<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../_include/head.php") ?>
    <link rel="stylesheet" href="bandeja.css">
    <title>BANDEJA DE EXPEDIENTES</title>
</head>
<body>
       
    <main id="main" class="main">
        <?php  include("../_include/header.php") ?>
        <section class="layout cleaner">
            <?php include("../_include/aside.php") ?>
            <section class="layout-content">
                
                <div class="inbox cleaner">
                    <div class="inbox-header">
                        <div class="layout-title">Bandeja de Mensajes</div>
                       
                    </div>
                    <div class="inbox-body">
                       
                       <div class="steps">
                           <div class="step-option">
                               <div class="step-circle">
                                   <div class="step-circle-icon">
                                       <img src="images/icons/step-tipo.svg" alt="">
                                   </div>
                               </div>
                               <div class="step-text">Tipo</div>
                           </div>
                       </div>

                       <div class="steps-content">
                           <h2>1. TIPO</h2>
                           <div class="steps-saludo">
                                <div class="steps-saludo-title">Bienvenido <strong>Huber</strong></div>
                                <div class="steps-saludo-subtitle">¿Qué tipo de acceso deseas crear?</div>
                           </div>
                           <div class="steps-options">
                               <label for="radio-test" class="form-radio">
                                    <input type="radio" class="form-radio-input" name="radio-test" id="radio-test" checked="">
                                    <label for=""> Empresa / Persona con negocio  </label>  
                                </label>
                           </div>
                       </div>


                    </div>
                </div>
            </section>
        </section>
    </main>

    <!-- Modal Crear documento -->
    <wc-modal-crear-documento class="modal-content" >
        <form action="" class="formulario">
            <a onclick="(function(){ fn.modal.close('wc-modal-crear-documento') })()" class="modal-content--is-close cleaner" href="javascript:void(0)"><i class="material-icons modal-content-icon--is-close">cancel</i></a>
    
            <div class="modal-head cleaner">
                <strong class="modal-head-title">Crear Documento</strong>
            </div>
            <div class="modal-body">
                <div class="row-fluid cleaner">
                    <div class="col-sm-6 cleaner">
                        <div class="form-double cleaner">
                            <div class="form-double-search cleaner">
                                <label for="User" class="form-label" required>RUC:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                            <button class="button button-terciary" >Validar</button>
                        </div>
                    </div>
                    <div class="col-sm-6 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label" required>Razón Social:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid cleaner">                
                    <div class="col-sm-6 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Correo electrónico:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
    
                    <div class="col-sm-6 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Número de celular:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid cleaner">
                    <div class="col-sm-6 cleaner">
                        <div class="form-double cleaner">
                            <div class="form-double-search cleaner">
                                <label for="User" class="form-label">DNI Rep. legal:</label>
                                <div class="form-group-input cleaner">
                                    <input type="text" class="form-input" name="User" id="User">
                                </div>
                            </div>
                            <button class="button button-terciary" id="ValidarDNI">Validar</button>
                        </div>
                    </div>
                    <div class="col-sm-6 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Representante Legal:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
    
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Logo empresa: ( Max 400kb .jpg )</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                    
    
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="departamento" class="form-label">Pregunta secreta:</label>
                            <label class="form-group-select " for="departamento">
                                <select id="departamento" name="options">
                                    <option value="">--Elegir--</option>
                                    <option value="1">Nombre de mascota</option>
                                    <option value="1">Colegio de secundaria</option>
                                </select>
                            </label>
                        </div>
                    </div>
    
                    <div class="col-sm-4 cleaner">
                        <div class="form-group cleaner m-b-20">
                            <label for="User" class="form-label">Respuesta de pregunta:</label>
                            <div class="form-group-input cleaner">
                                <input type="text" class="form-input" name="User" id="User">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-content cleaner">
                <button onclick="(function(){ fn.modal.close('wc-modal-crear-documento') })()" type="button" class="button button-secondary"><i class="material-icons">undo</i>Cancelar</button>
                <button type="button" class="button button-primary" ><i class="material-icons">highlight_off</i>Guardar</button>
            </div>
        </form>
    </wc-modal-crear-documento>
    
    
    <?php include '../_include/footer.php' ?>    
</body>
</html>