"use strict"

//Play Video
let video = document.getElementById("asideVideo");

document.body.addEventListener("click", showVideo );
function showVideo(e){
    video.play()
    e.target.removeEventListener("click", showVideo);
}


//Cerrar sesion
document.getElementById("CerrarSesion").addEventListener("click", function(e){
    e.preventDefault();
    
    let oParams = {
        type: "info",
        title: "ALERTA DE SISTEMA",
        desc: "Está seguro que quiere cerrar su sesión del sistema",
        ok : function(){
            alert("ok")
        },
        cancel : function(){
            alert("cancel")
        }
    }
    fn.alert.create(oParams,'modal') 
}, false)

// Reponsive para el Sidebar
let EventoSidebar  = function(ev){
    const main = document.getElementById('main');
    const mobile =  main.classList.contains('mobile')
    if(!mobile){
        if(main.classList.contains('mais--is-collapsed')){
            main.classList.remove('mais--is-collapsed')
            ev.target.innerHTML = 'menu'
        }
        else{
            main.classList.add('mais--is-collapsed')
            ev.target.innerText = 'sort'
        }
    }else{
        if(main.classList.contains('mais--is-slidedown')){
            main.classList.remove('mais--is-slidedown')
            ev.target.innerHTML = 'menu'
        }
        else{
            main.classList.add('mais--is-slidedown')
            ev.target.innerText = 'sort'
        }
    }
    
}
// Toggle del icono menu reponsive
window.onload = function() {
    document.getElementsByClassName('icon-menu').innerHTML = "menu"
    document.getElementById('main').classList.remove('mais--is-collapsed')

    if(window.innerWidth < 768){
        document.getElementById('main').classList.add('mobile')
    }else{
        document.getElementById('main').classList.remove('mobile')
    }
}
// Resize
window.onresize = function(ev) {
    document.getElementsByClassName('icon-menu').innerHTML = "menu"    
    document.getElementById('main').classList.remove('mais--is-collapsed')
    
    if(ev.target.innerWidth < 768){
        document.getElementById('main').classList.add('mobile')
    }else{
        document.getElementById('main').classList.remove('mobile')
    }
};

// Ver detalle de cada tabla
if(document.querySelectorAll('.inbox-table-dropdown') != null){        
    let dropDown = document.querySelectorAll('.inbox-table-dropdown');
    dropDown.forEach(function(el) {
        el.addEventListener('click', function(){

            let tableDetails = this.closest('.inbox-table-tbody-tr').nextElementSibling		

            if(tableDetails.classList.contains('inbox-table-details--is-open')){
                el.classList.remove('inbox-table-dropdown--is-open')
                tableDetails.classList.remove('inbox-table-details--is-open')
            }
            else{
                el.classList.add('inbox-table-dropdown--is-open')
                tableDetails.classList.add('inbox-table-details--is-open')
            }
        });
    });
}

// Ver opciones con la bolita
if(document.querySelectorAll('.table-icons--is-icon') != null){    
    let moreOptions = document.querySelectorAll('.table-icons--is-icon');
    moreOptions.forEach(function(el) {
        el.addEventListener('click', function(){
            let tableDetails = this.closest('.table-icons').nextElementSibling		

            if(tableDetails.classList.contains('table-options-ul--is-open')){
                tableDetails.classList.remove('table-options-ul--is-open')
            }
            else{
                tableDetails.classList.add('table-options-ul--is-open')
            }
        });
    });
}



if(document.querySelectorAll('.form-textarea') != null) {
    let textarea = document.querySelectorAll('.form-textarea');
    textarea.forEach(function(el) {
        if(el.hasAttribute("maxlength")){
            let maxLength = parseInt(el.getAttribute("maxlength"))
            let baseLength = 0

            document.addEventListener('DOMContentLoaded', function(){                
                const maxLengthHtml =  '<strong class="form-maxlength">'+baseLength + '/' + maxLength+'</strong>'
                const formInput = el.closest('.form-group-input')
                formInput.insertAdjacentHTML('beforeend', maxLengthHtml);
            })

            el.addEventListener('input', function(){
                baseLength = el.value.length
                el.nextElementSibling.textContent  = baseLength + '/' + maxLength
            })
        }
    });
}




// Poner el asterístico para todo los importantes
if(document.querySelectorAll('[required]') != null) {
    let required = document.querySelectorAll('[required]');
    required.forEach(function(el) {
        el.innerHTML = el.textContent + '<i class="required">*</i>'
    })
}