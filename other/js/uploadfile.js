 

class FileUploader {
    constructor(id, template){
        this.addStyles()
        this.time = 3000
        this.form = document.getElementById(id);
        this.uploadTemplate = document.getElementById(template);
        this.initialise();
    }


    addStyles = function(){
        const link = document.createElement('link');
        link.rel = 'stylesheet';  
        link.type = 'text/css';
        link.href = '../../css/uploadfile.css';  
        document.getElementsByTagName('head')[0].appendChild(link);
    }

    initialise = function(){
        this.constructTemplate();
        
        this.inputsWrapper = this.form.querySelector('.inputs');
        this.uploadCaption = this.form.querySelector('.uploadCaption');
        this.uploadItems = this.form.querySelector('#uploadItems');
        
        this.uploadCaption.addEventListener('click', this.handleAddNewFile.bind(this), false);
        this.uploadCaption.addEventListener('dragover', this.handleDragOverEnter.bind(this), false);
        this.uploadCaption.addEventListener('dragenter', this.handleDragOverEnter.bind(this), false);
        this.uploadCaption.addEventListener('drop', this.handleDragDropNewFile.bind(this), false);
    };

    constructTemplate = function(){
        this.form.innerHTML += this.uploadTemplate.innerHTML
    };

    handleNewFileSelection = function(){
        if (!this.fileInput || this.fileInput[this.fileInput.length - 1].files.length >= 1) {
            var input = document.createElement('input');
            input.setAttribute('class', 'fileInput');
            input.setAttribute('type', 'file');
            input.setAttribute('multiple', 'multiple');
            
            this.inputsWrapper.appendChild(input);
            input.addEventListener('change', this.handleAddedFile.bind(this), false);
        }
    };

    handleAddedFile = function(e){
        var files = e.dataTransfer ? e.dataTransfer.files : e.target.files;
        
        if (files) {
            var queueItem;
            for (var i = 0, numOfFiles = files.length; i < numOfFiles; i++) {
                queueItem = this.addQueueItem(files[i]);
                this.validateFile(queueItem, files[i]);
            }
        }

        // ver los archivos subidos
        console.log(files);
    };
    
    handleAddNewFile = function(e){
        this.handleNewFileSelection();
        this.fileInput = this.inputsWrapper.querySelectorAll('.fileInput');
        this.fileInput[this.fileInput.length - 1].click();
        e.preventDefault();
    };

    handleDragDropNewFile = function(e){
        e.stopPropagation();
        e.preventDefault();
        this.handleAddedFile(e);
    };


    handleDragOverEnter = function(e) {
        e.stopPropagation();
        e.preventDefault();
    };

    addQueueItem = function(file){
        // Add queue item wrapper
        var queueItem = document.createElement('div');
        queueItem.className += ' queueItem';
        
        // Generate File upload text
        var newUpload = document.createElement('p'),            
            fileSize = Math.round(file.size / 1000),
            sizeType = (fileSize > 1000) ? 'mb' : 'kb',
            progressBar = this.addProgressBar();
        newUpload.setAttribute('class', ' uploading');
        newUpload.innerText = "Subiendo: "+file.name + " | "+fileSize+sizeType;
        
        queueItem.appendChild(newUpload);
        queueItem.appendChild(progressBar);
        
        this.uploadItems.appendChild(queueItem);
        return queueItem;
    };


    addProgressBar = function(){
        var progressBarWrapper = document.createElement('div'),
            progressBar = document.createElement('div');
        
        progressBarWrapper.setAttribute('class', 'progressBar');
        progressBar.setAttribute('class', 'bar');
        progressBarWrapper.appendChild(progressBar);
        
        this.uploadItems.appendChild(progressBarWrapper);
        return progressBarWrapper;
    };


    validateFile = function(queueItem, file){
        if (file.type.match(/image/) || file.type === 'application/pdf') {
            this.simulateUpload(queueItem, file);
        } else {
            var errorTxt = document.createElement('p');
            errorTxt.className += ' error-txt';
            errorTxt.innerText = file.name+' es inválido.';
            queueItem.innerHTML = '';
            queueItem.appendChild(errorTxt);
        }
    };


    simulateUpload = function(queueItem, file){
        console.log(file)
        if (queueItem && file) {
            queueItem.className += ' simulatedProgress';
            setTimeout(function(){
                queueItem.className += ' resolved';
                var txt = queueItem.getElementsByTagName('p');
                txt[0].innerText = file.name+' se subió correctamente.';
                txt[0].className += ' success-txt';
                
                var tmpModal = `<div class="inbox-body upload-table">
                                    <table class="inbox-table cleaner w-100">                          
                                        <tbody class="inbox-table-tbody">
                                          <tr class="inbox-table-tbody-tr">
                                              <td class="inbox-table-tbody-td w-70"><a href="javascript:void(0)" class="link">`+file.name+`</a></td>
                                              <td class="inbox-table-tbody-td w-10">21/01/2019</td>
                                              <td class="inbox-table-tbody-td w-10">`+file.size+`</td>                                  
                                              <td class="inbox-table-tbody-td w-10"><a href="javascript:void(0)" class="link" id="deleteFile" data-key="12"><i class="material-icons">delete</i></a> </td>
                                          </tr>
                                        </tbody>
                                    </table>
                                </div>`

                txt[0].innerHTML = tmpModal;



                document.querySelector("#deleteFile").addEventListener('click', function() {
                    let key =  this.getAttribute('data-key');
                    //aqui funcionaldad para eliminar
                });


            }, this.time);
        }
    };


   

}











