//DOM
const $ = document.querySelector.bind(document);

//APP
let App = {};
App.init = (function() {
	//Init
	const $ = document.querySelector.bind(document);
    function handleFileSelect(evt) {
        const files = evt.target.files; // FileList object

     let fileBuffer=[];
      Array.prototype.push.apply( fileBuffer, files );

        $("#drop").classList.add("hidden");
        $("footer").classList.add("hasFiles");
     
 listfile(fileBuffer)

     
    }

	// drop events
	$("#drop").ondragleave = evt => {
		$("#drop").classList.remove("active");
		evt.preventDefault();
	};
	$("#drop").ondragover = $("#drop").ondragenter = evt => {
		$("#drop").classList.add("active");
		evt.preventDefault();
	};
	$("#drop").ondrop = evt => {
		$(".listado").files = evt.dataTransfer.files;
		$("footer").classList.add("hasFiles");
		$("#drop").classList.remove("active");
		evt.preventDefault();
	};

	//upload more
	function listfile(files){
		   Object.keys(files).forEach(file => {
		    if(files[file].size >  20000000) {
            	 setTimeout(() => { 
                   $("#max").classList.add("max");
                 $("#max").innerHTML =`<img src="assets/images/adobe-pdf-icon.svg" alt="Maxima capacidad"><span>su archivo ${files[file].name} excede los<span class="span">20 MB</span> máximos permitidos</span>`;
    }, 1000);
            }
           
      if (files[file].type=== "application/pdf") {

        setTimeout(() => {
          $(".list-files").innerHTML = `${Object.keys(files)
            .map(file => `<div class="file file--${file}">
            <div class="name"><span>${files[file].name}</span></div>
            <div class="progress active"></div>
            <div class="done">
                <div class="borrar">
                    <svg viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.2058 6.98941C12.0222 6.98941 11.8746 7.13701 11.8746 7.32061V13.1094C11.8746 13.6566 11.43 14.1012 10.8828 14.1012H6.91381C6.36661 14.1012 5.92201 13.6566 5.92201 13.1094V7.31881C5.92201 7.13521 5.77441 6.98761 5.59081 6.98761C5.40901 6.98761 5.25961 7.13521 5.25961 7.31881V13.1076C5.25961 14.0202 6.00121 14.7618 6.91381 14.7618H10.8828C11.7954 14.7618 12.537 14.0202 12.537 13.1076V7.31881C12.537 7.13701 12.3876 6.98941 12.2058 6.98941Z" fill="#ec2e2e"/>

                        <path d="M7.57439 13.1058V8.47982C7.57439 8.29622 7.42679 8.14862 7.24319 8.14862C7.06139 8.14862 6.91199 8.29622 6.91199 8.47982V13.1058C6.91199 13.2876 7.05959 13.437 7.24319 13.437C7.42679 13.437 7.57439 13.2876 7.57439 13.1058Z" fill="#ec2e2e"/>

                        <path d="M9.22859 13.1058V8.47982C9.22859 8.29622 9.08099 8.14862 8.89739 8.14862C8.71379 8.14862 8.56619 8.29622 8.56619 8.47982V13.1058C8.56619 13.2876 8.71379 13.437 8.89739 13.437C9.08099 13.437 9.22859 13.2876 9.22859 13.1058Z" fill="#ec2e2e"/>

                        <path d="M10.8828 13.1058V8.47982C10.8828 8.29622 10.7352 8.14862 10.5516 8.14862C10.368 8.14862 10.2204 8.29622 10.2204 8.47982V13.1058C10.2204 13.2876 10.368 13.437 10.5516 13.437C10.7352 13.437 10.8828 13.2876 10.8828 13.1058Z" fill="#ec2e2e"/>

                        <path d="M12.4578 4.21383H9.81179V3.87003C9.81179 3.32283 9.36719 2.87823 8.81999 2.87823C8.2728 2.87823 7.8282 3.32283 7.8282 3.87003V4.21383H5.1822C4.635 4.21383 4.1904 4.65843 4.1904 5.20563V5.86803C4.1904 6.04983 4.338 6.19923 4.5216 6.19923H13.1202C13.3038 6.19923 13.4514 6.05163 13.4514 5.86803V5.20563C13.4496 4.66023 13.005 4.21383 12.4578 4.21383ZM8.48879 3.87183C8.48879 3.69003 8.63639 3.54063 8.81999 3.54063C9.00179 3.54063 9.1512 3.69003 9.1512 3.87183V4.21563H8.4906L8.48879 3.87183ZM12.789 5.53683H4.851V5.20563C4.851 5.02383 4.9986 4.87443 5.1822 4.87443H12.4578C12.6396 4.87443 12.789 5.02203 12.789 5.20563V5.53683Z" fill="#ec2e2e"/>

                        <path d="M8.82 0C3.9492 0 0 3.9492 0 8.82C0 13.6908 3.9492 17.64 8.82 17.64C13.6908 17.64 17.64 13.6908 17.64 8.82C17.64 3.9492 13.6908 0 8.82 0ZM8.82 16.8714C4.374 16.8714 0.7686 13.266 0.7686 8.82C0.7686 4.374 4.374 0.7686 8.82 0.7686C13.266 0.7686 16.8714 4.374 16.8714 8.82C16.8714 13.266 13.266 16.8714 8.82 16.8714Z" fill="#ec2e2e"/>
                    </svg>
                </div>
            </div>
          
            </div>`)
            .join("")}`;
        }, 1000);
            let load = 2000 + (file * 2000); // fake load
            setTimeout(() => {
                $(`.file--${file}`).querySelector(".progress").classList.remove("active");
                $(`.file--${file}`).querySelector(".done").classList.add("anim");
 $(`.file--${file}`).querySelector(".borrar").addEventListener("click", () => {
  

  deletefile(files,file);
    
                   $("#max").classList.remove("max");
                 $("#max").innerHTML =``;
    
  listfile(files)
  });
            }, load);

        }else  {  
            setTimeout(() => { 
                $(".list-files").innerHTML = '<span class="span"> solo se permiten archivos PDF</span>                                       ' ; 
                
    }, 1000);
    }


});
	}
function deletefile(fileBuffer,file){
return	delete fileBuffer[file];
}
	// input change
	$(".listado").addEventListener("change", handleFileSelect);

})();


