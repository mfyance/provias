"use strict"

const tabsButton = document.querySelector(".form-head");
const tabs = tabsButton.querySelectorAll(".form-head-li");
const liActive = "form-head-li--is-active";
	
    tabs.forEach(function(el) {    	
   		el.addEventListener('click', function(event){   			
   			showTabs(this.getAttribute('tab-num'))
   		})
   	})

   	function showTabs(opt){
		document.querySelector("[tab-num='"+opt+"']").classList.add(liActive)
		document.querySelector("[layout-num='"+opt+"']").classList.remove('hidden')
   		switch(opt){
   			case 'extranjero':
   				document.querySelector("[tab-num='nacional']").classList.remove(liActive)
				document.querySelector("[layout-num='nacional']").classList.add('hidden')   				
   				break;

   			case 'nacional':
   				document.querySelector("[tab-num='extranjero']").classList.remove(liActive)
   				document.querySelector("[layout-num='extranjero']").classList.add('hidden')
   				break;

   		}
   	}

