"use strict"


let profile = document.getElementById('Profile')
let profileClose = document.getElementById('ProfileClose')
let profileContent = document.getElementsByTagName('profile')

//Abrir sidebar de usuario
profile.addEventListener('click', function(event){
	if(profileContent[0].getAttribute('profile-show') == "true"){
		profileContent[0].setAttribute("profile-show", "false")
	}else{
		profileContent[0].setAttribute("profile-show", "true")
	}
})

//Cerrar sidebar de usuario
profileClose.addEventListener('click', function(){
	profileContent[0].setAttribute("profile-show", "false")
})


//Seleccionar todos los mensajes
let checkboxes = document.getElementsByName('mailbox-checks');
let button = document.getElementById('checkAll');
let mails = document.querySelectorAll('[mailbox-card-check]');

button.addEventListener('click', function(){

	if(button.checked == false ){
		mails.forEach(function(el) {
	        el.setAttribute("mailbox-card-check", "false")
	    })
	    checkboxes.forEach(function(el) {
	        el.checked = false
	    })
	}else{
		mails.forEach(function(el) {
	        el.setAttribute("mailbox-card-check", "true")
	    })
	    checkboxes.forEach(function(el) {
	        el.checked = true
	    })
		
	}
})
